import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';
import Img from 'react-bootstrap/Image'
import {
  getProduct,
  getProductId,
} from "../services/api.js";

const Products = () => {
  const [products, getProducts] = useState([]);
  const [modals, getModals] = useState({});
  const [objs, getObjs] = useState([]);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = (e) => {
    setShow(true);
    getProductId(e).then((response) => {
    getModals(response);
    getObjs(modals.images);
    console.log(objs, "objs");
  });

}
  useEffect(() => {
    getProduct().then((response) => {
    getProducts(response.products);
    console.log(response);
  });
}, []);


  return (
    <Container className='pt-3'>
      <Row>
        {products.slice(0, 20)?.map((product) => (
          <Col lg="3" key={product.id}>
          <Card style={{ width: '98%' }}>
            <Card.Img variant="top" src={product.thumbnail} style={{ height: '200px' }} />
            <Card.Body>
              <Card.Title>{product.brand}</Card.Title>
              <Card.Text>
                {product.description}
              </Card.Text>
              <div className="d-grid gap-2">
                <Button variant="primary" onClick={() => handleShow(product.id)} style={{witdh:"100%"}} className="btn-custom">Ver Más</Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
        
        ))}
      </Row>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Gallery</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        {objs?.map((image) => (
          <Img style={{height:"100px", width:"100px", padding:"2px"}} src={image}/>
        ))}

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default Products;
