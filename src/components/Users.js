import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, {Search} from 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit';
import {
  getUser,
} from "../services/api.js";

const Users = () => {
  const [users, getUsers] = useState([]);
  const { SearchBar } = Search;
  useEffect(() => {
    getUser().then((response) => {
    getUsers(response.users);
    console.log(response)
  });
}, []);


  return (
    <Container>
      <Row>
          <Col lg="12">
          <Card><Card.Body>
              <ToolkitProvider
                    data={users}
                    keyField="firstName"
                    columns={[
     
                      {
                        dataField: "firstName",
                        text: "FIRSTNAME",
                        sort: false,
                      },
                      {
                        dataField: "lastName",
                        text: "LASTNAME",
                        sort: false,
                      },
                      {
                        dataField: "age",
                        text: "AGE",
                        sort: false,
                      },
                      {
                        dataField: "ip",
                        text: "IP",
                        sort: false,
                      },
                    ]}
                    search
                  >
                    {(props) => (
                      <div className="py-4">
                        <div
                          id="datatable-basic_filter"
                          className="dataTables_filter px-4 pb-1"
                        >
                          <label>
                            Buscar:
                            <SearchBar
                              className="form-control-sm"
                              placeholder=""
                              {...props.searchProps}
                            />
                          </label>
                        </div>
                        <BootstrapTable
                          {...props.baseProps}
                          pagination={paginationFactory()}
                          bordered={false}
                        />
                      </div>
                    )}
              </ToolkitProvider>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Users;
