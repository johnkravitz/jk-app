import React from 'react';
import {BrowserRouter as Router, Routes, Route, Link} from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
// Pages import Here 
import ProductDefault from "./components/Products.js";
import UserDefault from "./components/Users.js";
import './App.css';
import './assets/css/custom.css';

function App() {
  return (
    <Router>
      <div>

        <Navbar bg="dark" variant="dark">
          <Container>
            <Navbar.Brand href="/">CPANAX- DESAFIO</Navbar.Brand>
            <Nav>
              <Link to="/" className='btn btn-light text-black'>Products</Link>
              <Link to="/users" className='btn btn-light text-black'>Users</Link>
            </Nav>
          </Container>
        </Navbar>
          <Routes>
            <Route path="/" element={<ProductDefault />} />
            <Route path="/users" element={<UserDefault />} />
          </Routes>
      </div>
    </Router>
  );
}

export default App;
