"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getProduct = getProduct;
exports.getProductId = getProductId;
exports.getUser = getUser;

var _setting = require("./setting");

var ENDPOINT = "".concat(_setting.BASE_URL, "/products");
var ENDPOINTUSER = "".concat(_setting.BASE_URL, "/users");

function getProduct() {
  var token, response, products;
  return regeneratorRuntime.async(function getProduct$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          token = localStorage.usertoken;
          _context.next = 3;
          return regeneratorRuntime.awrap(fetch("".concat(ENDPOINT), {
            method: "GET",
            headers: {
              "Content-type": "application/json; charset=UTF-8",
              Accept: "application/json",
              Authorization: "Bearer ".concat(token)
            }
          }));

        case 3:
          response = _context.sent;
          _context.next = 6;
          return regeneratorRuntime.awrap(response.json());

        case 6:
          products = _context.sent;
          return _context.abrupt("return", products);

        case 8:
        case "end":
          return _context.stop();
      }
    }
  });
}

function getProductId(id) {
  var token, response, products;
  return regeneratorRuntime.async(function getProductId$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          token = localStorage.usertoken;
          _context2.next = 3;
          return regeneratorRuntime.awrap(fetch("".concat(ENDPOINT, "/") + id, {
            method: "GET",
            headers: {
              "Content-type": "application/json; charset=UTF-8",
              Accept: "application/json",
              Authorization: "Bearer ".concat(token)
            }
          }));

        case 3:
          response = _context2.sent;
          _context2.next = 6;
          return regeneratorRuntime.awrap(response.json());

        case 6:
          products = _context2.sent;
          return _context2.abrupt("return", products);

        case 8:
        case "end":
          return _context2.stop();
      }
    }
  });
}

function getUser() {
  var token, response, users;
  return regeneratorRuntime.async(function getUser$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          token = localStorage.usertoken;
          _context3.next = 3;
          return regeneratorRuntime.awrap(fetch("".concat(ENDPOINTUSER), {
            method: "GET",
            headers: {
              "Content-type": "application/json; charset=UTF-8",
              Accept: "application/json",
              Authorization: "Bearer ".concat(token)
            }
          }));

        case 3:
          response = _context3.sent;
          _context3.next = 6;
          return regeneratorRuntime.awrap(response.json());

        case 6:
          users = _context3.sent;
          return _context3.abrupt("return", users);

        case 8:
        case "end":
          return _context3.stop();
      }
    }
  });
}