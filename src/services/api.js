import { BASE_URL } from "./setting";

const ENDPOINT = `${BASE_URL}/products`;
const ENDPOINTUSER = `${BASE_URL}/users`;

export async function getProduct() {
  const token = localStorage.usertoken;
  const response = await fetch(`${ENDPOINT}`, {
    method: "GET",
    headers: {
        "Content-type": "application/json; charset=UTF-8",
        Accept: "application/json",
        Authorization: `Bearer ${token}`,
    },
  });

  const products = await response.json();
  return products;
}
export async function getProductId(id) {
    const token = localStorage.usertoken;
    const response = await fetch(`${ENDPOINT}/` + id, {
      method: "GET",
      headers: {
          "Content-type": "application/json; charset=UTF-8",
          Accept: "application/json",
          Authorization: `Bearer ${token}`,
      },
    });
  
    const products = await response.json();
    return products;
  }

export async function getUser() {
    const token = localStorage.usertoken;
    const response = await fetch(`${ENDPOINTUSER}`, {
      method: "GET",
      headers: {
          "Content-type": "application/json; charset=UTF-8",
          Accept: "application/json",
          Authorization: `Bearer ${token}`,
      },
    });
  
    const users = await response.json();
    return users;
  }